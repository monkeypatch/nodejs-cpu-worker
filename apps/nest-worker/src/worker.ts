import {worker} from "workerpool";
import {computation} from "./computation/computation";

async function bootstrap() {
  worker({
    computation: () => computation()
  })
}

bootstrap();
