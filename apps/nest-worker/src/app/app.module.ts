import {Module} from '@nestjs/common';

import {AppController} from './app.controller';
import {WorkerLauncherService} from "./worker-launcher.service";

@Module({
  imports: [],
  controllers: [AppController],
  providers: [WorkerLauncherService],
})
export class AppModule {
}
