import {Injectable} from "@nestjs/common";
import {pool, WorkerPool} from 'workerpool';
import * as path from "path";

@Injectable()
export class WorkerLauncherService {
  private workerPool: WorkerPool;

  constructor() {
    this.workerPool = pool(path.join(__dirname, "worker.js"), {
      maxWorkers: 8, workerType: "thread", workerThreadOpts: {
        resourceLimits: {
          codeRangeSizeMb: 4,
          maxYoungGenerationSizeMb: 8,
          maxOldGenerationSizeMb: 32,
          stackSizeMb: 0.4
        }
      }
    })
  }

  async compute() {
    return await this.workerPool.exec<() => number>('computation', null);
  }
}
