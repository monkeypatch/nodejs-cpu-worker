import {Controller, Get} from '@nestjs/common';
import {WorkerLauncherService} from "./worker-launcher.service";
import {computation} from "../computation/computation";

@Controller()
export class AppController {
  constructor(
    private readonly workerService: WorkerLauncherService) {
  }

  @Get('computation')
  async getData() {
    return {data: computation()};
  }

  @Get('worker')
  async getDataWithWorker() {
    return {data: await this.workerService.compute()}
  }

  @Get('health')
  healthCheck() {
    return {status: 'healthy'};
  }
}
