export function computation(nPoints = 1_000_000): number {
  let n = 0;
  for (let i = 0; i < nPoints; i++) {
    const x = Math.random();
    const y = Math.random();
    if (x ** 2 + y ** 2 < 1) {
      n += 1;
    }
  }
  return 4 * n / nPoints;
}
