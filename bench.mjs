#!/usr/bin/env zx

await $`mkdir -p benches`;

const jobs = [
  //$`autocannon -n -j http://localhost:3000/api/computation > 'benches/computation.json'`,
  $`autocannon -n -j http://localhost:3000/api/worker > 'benches/computation.json'`,
  $`autocannon -n -j -R1 http://localhost:3000/api/health >  'benches/health.json'`
];

await Promise.all(jobs);

const computation = await fs.readJson('./benches/computation.json');
const health = await fs.readJson('./benches/health.json');

const result = {
  'computation_request_rate_average (req/s)': computation.requests.average,
  'computation_request_rate_stddev (req/s)': computation.requests.stddev,
  'health_latency_average (ms)': health.latency.average,
  'health_latency_stddev (ms)': health.latency.stddev
}

console.table(result);
